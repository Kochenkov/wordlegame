package com.vkochenkov.wordlegame.presentation.theme

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import com.vkochenkov.wordlegame.model.Cell

@Composable
fun MainButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    elevation: ButtonElevation? = ButtonDefaults.elevation(),
    shape: Shape = MaterialTheme.shapes.small,
    border: BorderStroke? = null,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = WordleTheme.colors.button,
        contentColor = WordleTheme.colors.content
    ),
    contentPadding: PaddingValues = ButtonDefaults.ContentPadding,
    content: @Composable RowScope.() -> Unit
) {
    Button(
        onClick = onClick,
        modifier = modifier,
        enabled = enabled,
        interactionSource = interactionSource,
        elevation = elevation,
        shape = shape,
        border = border,
        colors = colors,
        contentPadding = contentPadding,
        content = content
    )
}

@Composable
fun BoardCell(
    cell: Cell,
    modifier: Modifier
) {
    var cellSize by remember { mutableStateOf(Size.Zero) }

    Card(
        backgroundColor = when (cell.status) {
            Cell.Status.PRESENT -> Yellow
            Cell.Status.WRONG -> WordleTheme.colors.wrongCell
            Cell.Status.RIGHT -> Green
            else -> WordleTheme.colors.emptyCell
        },
        modifier = modifier
            .aspectRatio(1f)
            .padding(2.dp)
            .border(BorderStroke(2.dp, WordleTheme.colors.button), MaterialTheme.shapes.medium)
            .onGloballyPositioned { coordinates ->
                cellSize = coordinates.size.toSize()
            }
    ) {
        Box(
            contentAlignment = Alignment.Center
        ) {
            if (cell.letter != null) {
                Text(
                    fontSize = (cellSize.height/5).sp,
                    text = cell.letter.toString(),
                    color = WordleTheme.colors.content
                )
            }
        }
    }
}