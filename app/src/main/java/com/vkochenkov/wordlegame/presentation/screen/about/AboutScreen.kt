package com.vkochenkov.wordlegame.presentation.screen.about

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.vkochenkov.wordlegame.R
import com.vkochenkov.wordlegame.model.Cell
import com.vkochenkov.wordlegame.presentation.theme.BoardCell
import com.vkochenkov.wordlegame.presentation.theme.WordleTheme

@Composable
fun AboutScreen() {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = WordleTheme.colors.background)
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            fontSize = 16.sp,
            text = stringResource(R.string.game_rules)
        )
        Row(Modifier.padding(top = 8.dp)) {
            arrayOf(
                Cell('e', Cell.Status.RIGHT),
                Cell('a', Cell.Status.WRONG),
                Cell('r', Cell.Status.PRESENT),
                Cell('l', Cell.Status.PRESENT),
                Cell('y', Cell.Status.WRONG)
            ).forEach {
                BoardCell(it, Modifier.weight(1f))
            }
        }
        Card(
            Modifier
                .padding(top = 8.dp)
                .fillMaxWidth()) {
            Column(
                modifier = Modifier
                    .background(WordleTheme.colors.wrongCell)
                    .padding(8.dp),
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    BoardCell(Cell('e', Cell.Status.RIGHT), Modifier.size(50.dp))
                    Spacer(Modifier.padding(4.dp))
                    Text(stringResource(R.string.row_right))
                }
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    BoardCell(Cell('r', Cell.Status.PRESENT), Modifier.size(50.dp))
                    BoardCell(Cell('l', Cell.Status.PRESENT), Modifier.size(50.dp))
                    Spacer(Modifier.padding(4.dp))
                    Text(stringResource(R.string.row_present))
                }
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    BoardCell(Cell('a', Cell.Status.WRONG), Modifier.size(50.dp))
                    BoardCell(Cell('y', Cell.Status.WRONG), Modifier.size(50.dp))
                    Spacer(Modifier.padding(4.dp))
                    Text(stringResource(R.string.row_wrong))
                }
            }
        }
        Text(
            modifier = Modifier.padding(top = 8.dp),
            fontSize = 16.sp,
            text = stringResource(R.string.hidden_word_was)
        )
        Row(Modifier.padding(top = 8.dp)) {
            arrayOf(
                Cell('e', Cell.Status.RIGHT),
                Cell('l', Cell.Status.RIGHT),
                Cell('d', Cell.Status.RIGHT),
                Cell('e', Cell.Status.RIGHT),
                Cell('r', Cell.Status.RIGHT)
            ).forEach {
                BoardCell(it, Modifier.weight(1f))
            }
        }
    }
}